# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>

pkgname=dotnet7-stage0
pkgver=7.0.100_rc1
pkgrel=0

[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""
[ "$CHOST" != "$CTARGET" ] && _target="-$CTARGET_ARCH" || _target=""

_extra_nupkgs="
	https://globalcdn.nuget.org/packages/stylecop.analyzers.1.2.0-beta.435.nupkg
	https://globalcdn.nuget.org/packages/stylecop.analyzers.unstable.1.2.0.435.nupkg
	"

# Tag of tarball generator. This may change from usual due to mistakes from upstream
_gittag=v7.0.100-rc.1.22431.12
_llvmtag=c1304304028d603e34f6803b0740c34441f80d2e
_artifactsver=0.1.0-7.0.100-bootstrap.11
_bootstrapver="7.0.100-rc.1.22431.12"

# Version of packages that aren't defined in Version.Details.xml
_iltoolsver=7.0.0-rc.1.22403.8
_iltoolsver_suf=${_iltoolsver##*-}
_iltoolsver_pre=${_iltoolsver%%-*}
_iltoolsver_ns=$_iltoolsver
_iltoolsver_sh=$_iltoolsver
_llvmver=1.0.0-alpha.1.22411.1
_llvmver_suf=${_llvmver##*-}
_llvmver_pre=${_llvmver%%-*}
_llvmver_ns=$_llvmver
_llvmver_sh=$_llvmver
_installerver=${_gittag/v}
_installerver_suf=${_installerver##*-}
_installerver_pre=${_installerver%%-*}
_installerver_ns=$_installerver
_installerver_sh=$_installerver

# Patches to be used. String before '_' refers to repo to patch
# Look for patch notes within each patch for what they fix / where they come from
# build_* patches applies directly to $builddir
_patches="
	aspnetcore_musl-build-fix.patch
	aspnetcore_supress-ca1305.patch
	installer_arm-build.patch
	installer_musl-build-fix.patch
	installer_reprodicible-tarball.patch
	installer_runtimepacks.patch
	installer_set-local-repo.patch
	roslyn_revert-lift-version-codeanalysis.patch
	runtime_add-cmake-asm-workaround.patch
	sdk_dummyshim-fix.patch
	sdk_set-local-repo.patch
	"

_pkgver_macro=${pkgver%.*}
_pkgver_prior=1
_pkgver_name="${_pkgver_macro//[.0]}"
pkgdesc="The .NET Core stage0 Alpine cross-compilation aport"
arch="x86_64 aarch64 armv7"
url=https://www.microsoft.com/net/core
license="MIT"
subpackages="
	dotnet$_pkgver_name-stage0-artifacts:artifacts:noarch
	dotnet$_pkgver_name-stage0-bootstrap
	"
source="
	https://repo.gpg.nz/apk/archives/dotnet-${_gittag/release\/}.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-x64.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-x64.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-arm64.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-arm64.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-arm.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-arm.tar.gz
	Private.SourceBuilt.Artifacts.$_artifactsver.noextract::https://dotnetcli.azureedge.net/source-built-artifacts/assets/Private.SourceBuilt.Artifacts.$_artifactsver.tar.gz
	llvm-project-$_llvmtag.tar.gz::https://github.com/dotnet/llvm-project/archive/$_llvmtag.tar.gz
	dotnet-versiondetails-$_gittag.xml::https://raw.githubusercontent.com/dotnet/installer/$_gittag/eng/Version.Details.xml
	$_extra_nupkgs
	$_patches
	"
makedepends_host="
	bash
	binutils
	clang
	cmake
	findutils
	g++
	gcc
	grep
	icu-dev
	krb5-dev
	libintl
	libstdc++
	libunwind-dev
	libxml2-dev
	libxml2-utils
	linux-headers
	lld
	lttng-ust-dev
	musl-dev
	musl-utils
	openssl-dev
	pigz
	unzip
	zip
	zlib-dev
	"
makedepends_build="
	$makedepends_host
	binutils$_cross
	git
	gcc$_cross
	llvm
	llvm-dev
	python3
	"
[ "$CARCH" = "s390x" ] && makedepends_host="${makedepends_host/lld/}"
case $CARCH in
	x86_64) _dotnet_arch="x64";;
	aarch64) _dotnet_arch="arm64";;
	armv7) _dotnet_arch="arm";;
	armhf) _dotnet_arch="armv6";;
	*) _dotnet_arch="$CARCH";;
esac
case $CTARGET in
	x86_64*) _dotnet_target="x64";;
	aarch64*) _dotnet_target="arm64";;
	armv7*) _dotnet_target="arm";;
	armhf*) _dotnet_target="armv6";;
	*) _dotnet_target="$CTARGET";;
esac

builddir="$srcdir"/dotnet-${_gittag/release\/}
_packagesdir="$srcdir"/local-packages
_libdir="/usr/lib"
_nugetdir="$srcdir"/nuget
_downloaddir="$srcdir"/local-downloads
_cli_root="$srcdir"/bootstrap
_nuget="$_cli_root/dotnet nuget"
_outputdir="$srcdir"/artifacts

# generates tarball containing all components built by dotnet
snapshot() {
	local _pkg=$SRCDEST/${builddir##*/}.tar.gz
	if [ -d "$srcdir" ]; then cd "$srcdir"; else mkdir -p "$srcdir" && cd "$srcdir"; fi
	if [ -d "installer" ]; then cd "$srcdir"/installer; else git clone https://github.com/dotnet/installer && cd "$srcdir"/installer; fi
	git checkout $_gittag

	sed 's|/src/installer||' "$startdir"/installer_reprodicible-tarball.patch | patch -Np1 || true
	if [ ! -d "$_cli_dir" ]; then local _cli_dir=; fi

	_InitializeDotNetCli="$_cli_dir" DOTNET_INSTALL_DIR="$_cli_dir" DotNetBuildFromSource=true ./build.sh \
		/p:ArcadeBuildTarball=true \
		/p:TarballDir=$builddir \
		/p:TarballFilePath=$_pkg

	cd "$startdir"
	abuild checksum
}

prepare() {
	default_prepare

	# adjusts sdk version to expected
	sed "s|7.0.100-preview.7.22377.5|$_bootstrapver|" -i "$builddir"/src/roslyn/global.json
	sed "s|7.0.100-preview.7.22377.5|$_bootstrapver|" -i "$builddir"/src/sdk/global.json

	sed "s|@@PACKAGESDIR@@|$_packagesdir|" -i "$builddir"/src/sdk/NuGet.config
	sed "s|@@PACKAGESDIR@@|$_packagesdir|" -i "$builddir"/src/installer/NuGet.config

	# llvm unharcode clang version
	sed "s|-DCMAKE_C_COMPILER=clang-9|-DCMAKE_C_COMPILER=clang-14|" -i "$srcdir"/llvm-project-$_llvmtag/llvm.proj
	sed "s|-DCMAKE_CXX_COMPILER=clang++-9|-DCMAKE_CXX_COMPILER=clang++|" -i "$srcdir"/llvm-project-$_llvmtag/llvm.proj

	mkdir -p "$_cli_root"
	mkdir -p $_packagesdir $_downloaddir $_outputdir $_nugetdir

	tar --use-compress-program="pigz" -xf "$srcdir"/dotnet-sdk-$_pkgver_macro*$_dotnet_arch* -C "$_cli_root" --no-same-owner
}

_runtime() {
	local _runtimever=$(awk '{if($2=="Name=\"Microsoft.NETCore.App.Ref\""){print $3}}' "$srcdir"/dotnet-versiondetails-$_gittag.xml | sed 's|.*=||' | tr -d '"' | tr -d '>')
	local _runtimever_suf=${_runtimever##*-}
	local _runtimever_pre=${_runtimever%%-*}
	local _runtimever_ns=$_runtimever
	local _runtimever_sh=$_runtimever

	msg "Building runtime version $_runtimever and iltools version $_iltoolsver"
	cd "$builddir"/src/runtime

	local args="
		-c Release
		-arch $_dotnet_target
		-clang
		/p:NoPgoOptimize=true
		/p:EnableNgenOptimization=false
		/p:VersionSuffix=$_runtimever_suf
	"
	[ "$CBUILD" != "$CTARGET" ] && local args="$args -cross"
#	ROOTFS_DIR="$CBUILDROOT" ./build.sh $args

	local _iltools_array="
		runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.TestHost.$_runtimever_sh.nupkg
		runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.ILAsm.$_runtimever_sh.nupkg
		runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.ILDAsm.$_runtimever_sh.nupkg
	"
	local _push_array="
		Microsoft.NETCore.App.Host.linux-musl-$_dotnet_target.$_runtimever_sh.nupkg
		Microsoft.NETCore.App.Runtime.linux-musl-$_dotnet_target.$_runtimever_sh.nupkg
		Microsoft.NETCore.App.Crossgen2.linux-musl-$_dotnet_target.$_runtimever_sh.nupkg
		"
	local _download_array="
		dotnet-runtime-$_runtimever_sh-linux-musl-$_dotnet_target.tar.gz
		"
	local _output_array="
		dotnet-runtime-$_runtimever_sh-linux-musl-$_dotnet_target.tar.gz
		Microsoft.NETCore.App.Host.linux-musl-$_dotnet_target.$_runtimever_sh.nupkg
		Microsoft.NETCore.App.Runtime.linux-musl-$_dotnet_target.$_runtimever_sh.nupkg
		Microsoft.NETCore.App.Crossgen2.linux-musl-$_dotnet_target.$_runtimever_sh.nupkg
		runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.DotNetHost.$_runtimever_sh.nupkg
		runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.DotNetHostPolicy.$_runtimever_sh.nupkg
		runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.DotNetHostResolver.$_runtimever_sh.nupkg
		runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.DotNetAppHost.$_runtimever_sh.nupkg
		$_iltools_array
		"
	for i in $_output_array; do cp artifacts/packages/*/*/$i "$_outputdir"; done
	for i in $_push_array; do $_nuget push artifacts/packages/*/*/$i --source="$_packagesdir"; done
	mkdir -p $_downloaddir/Runtime/$_runtimever_ns
	for i in $_download_array; do cp artifacts/packages/*/*/$i $_downloaddir/Runtime/$_runtimever_ns; done

	# source-build expects a certain version of ilasm, ildasm and testhost
	# following adjusts version
	for i in $_iltools_array; do
		local nupkg="$_outputdir"/$i
		local nuspec=${nupkg/$_runtimever_sh.nupkg/nuspec}
		local nuspec=${nuspec##*/}
		echo $nupkg
		echo $nuspec
		echo $_runtimever_sh
		echo $_iltoolsver_sh
		unzip -p $nupkg $nuspec | sed "s|$_runtimever_sh|$_iltoolsver_sh|" > $nuspec
		zip -u $nupkg $nuspec
		mv $nupkg ${nupkg/$_runtimever_sh/$_iltoolsver_sh}
	done

}

_llvm() {
	msg "Building llvm-project version $_llvmver"
	cd "$srcdir"/llvm-project-$_llvmtag

	local args="
		-c Release
		-arch $_dotnet_target
		-clang
		--restore
		--build
		--pack
		/p:VersionSuffix=$_llvmver_suf
		/p:OSRid=linux-musl
		/p:LLVMTableGenPath=../../../../BuildRoot-$_dotnet_arch/bin/llvm-tblgen
		"
	# build tablegen for native arch
	if [ "$CBUILD" != "$CTARGET" ]; then
		./eng/build.sh -c Release -clang -arch $_dotnet_arch --restore --build /p:BuildLLVMTableGenOnly=true
		local args="$args -cross"
	fi
	CLANG_TARGET=$CTARGET ROOTFS_DIR="$CBUILDROOT" ./eng/build.sh $args
	cp artifacts/packages/Release/*/runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.Runtime.ObjWriter.$_llvmver_sh.nupkg $_outputdir
	cp artifacts/packages/Release/*/runtime.linux-musl-$_dotnet_target.Microsoft.NETCore.Runtime.JIT.Tools.$_llvmver_sh.nupkg $_outputdir
}

_roslyn() {
	local _roslynver=$(awk '{if($2=="Name=\"Microsoft.Net.Compilers.Toolset\""){print $3}}' "$srcdir"/dotnet-versiondetails-$_gittag.xml | sed 's|.*=||' | tr -d '"' | tr -d '>')
	local _roslynver_suf=${_roslynver##*-}
	local _roslynver_pre=${_roslynver%%-*}
	local _roslynver_ns=$_rosylnver
	local _roslynver_sh=$_rosylnver

	msg "Building roslyn version $_roslynver"
	cd "$builddir"/src/roslyn

	./eng/build.sh --restore --build --pack -c Release /p:VersionSuffix=$_roslynver_suf
	for I in artifacts/packages/*/*/*.nupkg; do 
		$_nuget push $I --source="$_packagesdir"
	done
}

_sdk() {
	local _sdkver=$(awk '{if($2=="Name=\"Microsoft.NET.Sdk\""){print $3}}' "$srcdir"/dotnet-versiondetails-$_gittag.xml | sed 's|.*=||' | tr -d '"' | tr -d '>')
	local _sdkver_suf=${_sdkver##*-}
	local _sdkver_pre=${_sdkver%%-*}
	local _sdkver_ns=$_sdkver
	local _sdkver_sh=$_sdkver

	msg "Building sdk version $_sdkver"
	cd "$builddir"/src/sdk

	cd ./src/Tests/ArgumentsReflector
	DotNetBuildFromSource=false dotnet restore /p:UseSharedCompilation=false
	DotNetBuildFromSource=false dotnet build --configuration Release
	cd ../../..

	local args="
		-c Release
		/p:Architecture=$_dotnet_target 
		/p:VersionSuffix=$_sdkver_suf
		/p:ArcadeBuildFromSource=true
		/p:ArcadeInnerBuildFromSource=true
		/p:CopyWipIntoInnerSourceBuildRepo=false
		/p:ContinuousIntegrationBuild=true
		/p:DisableCheckingDuplicateNuGetItems=true
		/p:UseSharedCompilation=false
	"
	./build.sh --pack $args /p:nowarn='"CA1420;NU5128;CS1591;CS2008"'
	mkdir -p "$_downloaddir"/Sdk/$_sdkver_ns
	cp artifacts/packages/*/*/dotnet-toolset-internal-*.zip "$_downloaddir"/Sdk/$_sdkver_ns
}

_aspnetcore() {
	local _aspnetver=$(awk '{if($2=="Name=\"Microsoft.AspNetCore.App.Ref\""){print $3}}' "$srcdir"/dotnet-versiondetails-$_gittag.xml | sed 's|.*=||' | tr -d '"' | tr -d '>')
	local _aspnetver_suf=${_aspnetver##*-}
	local _aspnetver_pre=${_aspnetver%%-*}
	local _aspnetver_ns=$_aspnetver
	local _aspnetver_sh=$_aspnetver

	msg "Build aspnetcore version $_aspnetver"
	cd "$builddir"/src/aspnetcore
	local args="
		--os-name linux-musl
		-arch $_dotnet_target
		-c Release
		-no-build-nodejs
		/p:DotNetAssetRootUrl=file://$_downloaddir/
		/p:VersionSuffix=$_aspnetver_suf
		"
	[ "$_dotnet_target" = "x86" ] && local="$args /p:CrossgenOutput=false"
	./eng/build.sh --pack $args
	$_nuget push artifacts/packages/*/*/Microsoft.AspNetCore.App.Runtime.linux-musl-$_dotnet_target.$_aspnetver_sh.nupkg --source=$_packagesdir
	mkdir -p "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
	cp artifacts/installers/*/aspnetcore-runtime-internal-$_aspnetver_sh-linux-musl-$_dotnet_target.tar.gz "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
	cp artifacts/installers/*/aspnetcore_base_runtime.version "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
	cp artifacts/packages/*/*/Microsoft.AspNetCore.App.Runtime.linux-musl-$_dotnet_target.$_aspnetver_sh.nupkg "$_outputdir"
}

_installer() {
	msg "Building installer version $_installerver"

	local args="
		-c Release
		/p:HostRid=linux-x64
		/p:CoreSetupBlobRootUrl=file://$_downloaddir/
		/p:DotnetToolsetBlobRootUrl=file://$_downloaddir/
		/p:VersionSuffix=$_installerver_suf
		"
	[ "$CBUILD" != "$CTARGET" ] && local args="$args /p:Architecture=$_dotnet_target"
	[ "$_dotnet_target" = "x86" ] && local args="$args /p:DISABLE_CROSSGEN=True"
	cd "$builddir"/src/installer
	./build.sh $args
	cp artifacts/packages/*/*/dotnet-sdk-$_installerver_sh-linux-musl-$_dotnet_target.tar.gz "$_outputdir"
	cp artifacts/packages/*/*/dotnet-sdk-$_installerver_sh-linux-musl-$_dotnet_target.tar.gz "$startdir"
}

build() {
	msg "Cross-building for $CARCH"
	
	export _InitializeDotNetCli=$_cli_root
	export DOTNET_INSTALL_DIR=$_cli_root
	export PATH="$_cli_root:$PATH"
	export NUGET_PACKAGES=$_nugetdir
	export DotNetBuildFromSource=true
	export SHELL=/bin/bash

	ulimit -n 4096

	_runtime
#	_llvm
#	_roslyn
#	_sdk
#	_aspnetcore
#	_installer
}

package() {
	# lua-aports / buildrepo doesn't know to always build stage0 first when dealing
	# with virtual packages. Thus, we need to depend on an empty stage0 pkg that
	# dotnetx-build will pull, thus forcing build of stage0 first
	mkdir -p "$pkgdir"
}

bootstrap() {
	# allows stage0 to be pulled by dotnetx-build if first build of dotnetx
	provides="dotnet$_pkgver_name-bootstrap"
	provider_priority=$_pkgver_prior

	install -dm 755 \
		"$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/docs \
		"$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/comp \
		"$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver

	# unpack build artifacts to bootstrap subdir for use by future builds
	tar --use-compress-program="pigz" \
		-xf "$_outputdir"/dotnet-sdk-$_pkgver_macro*.tar.gz \
		-C "$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/ \
		--no-same-owner

	# same for arch-specific extra nupkgs
	for i in $_extra_nupkgs; do
		[ -n "${i##*linux-musl-$_dotnet_target*}" ] && continue
		cp $srcdir/${i##*/} "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/.
	done

	# extract artifacts to artifacts dir for use by future dotnet builds
	cp "$_outputdir"/*.nupkg "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/

	# assemble completions
	install -m 755 "$builddir"/src/sdk/scripts/register-completions.bash "$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/comp/
	install -m 755 "$builddir"/src/sdk/scripts/register-completions.zsh "$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/comp/

	# assemble docs
	find "$builddir" -iname 'dotnet*.1' -type f -exec cp '{}' "$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/docs/ \;

	# See https://github.com/dotnet/source-build/issues/2579
	find "$subpkgdir" -type f -name 'testhost.x86' -delete
	find "$subpkgdir" -type f -name 'vstest.console' -delete
}

# build relies on a plethora of nupkgs which are provided by this Artifacts file.
# stage0 sources these from Microsoft, which then allows bootstrap to build
# locally hosted versions. The following unpacks built tarball into directory
# for use by future builds.
artifacts() {
	pkgdesc="Internal package for building .NET $_pkgver_macro Software Development Kit"
	# hack to allow artifacts to pull itself
	provides="dotnet$_pkgver_name-bootstrap-artifacts"
	provider_priority=$_pkgver_prior

	# directory creation
	install -dm 755 \
		"$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver \
		"$subpkgdir"/usr/share/licenses

	# extract artifacts to artifacts dir for use by future dotnet builds
	tar --use-compress-program="pigz" \
		-xf "$srcdir"/Private.SourceBuilt.Artifacts.*.noextract \
		-C "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/ \
		--no-same-owner \
		--exclude '*linux*'
	
	# same for extra non-arch nupkgs
	for i in $_extra_nupkgs; do
		[ -z "${i##*linux*}" ] && continue
		cp $srcdir/${i##*/} "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/.
	done

}

sha512sums="
772653765ec38ac987c4a896b82395e5b179269f50607abe396e5aa298e25618db864ce5bfc3c300b3d8b1ff5aff382cb1c5e8fd84093062fd83b210e7b8cc9d  dotnet-v7.0.100-rc.1.22431.12.tar.gz
f0b3836d1634a3910af7894c2201c40096c1c1f26f8c107fca9ea6601d2cf770cc88a1534f03d94d545be2d2217b04b95c5d3ddf52c11bf1d93457a184947298  dotnet-sdk-7.0.100-rc.1.22431.12-linux-musl-x64.noextract
6567ff07349dad9838e3d7f755944647ce89918e94e28bf3fd953606d6c0d845516ed09c3f113064203360d09b7abe3155542f5f5b2d3e6e43019c970e802e17  dotnet-sdk-7.0.100-rc.1.22431.12-linux-musl-arm64.noextract
05aec68fd6ab72a292e9163d495075b0ecd8657a89758bd79e48e7653373d7a7da15fad0dbe61b4453af82a4fe538c2c2e7e60c6dfd6c6837f3636679b5c1897  dotnet-sdk-7.0.100-rc.1.22431.12-linux-musl-arm.noextract
e4277597e56e0a7242e999c2ee4a795e930337d066326fa2c88c9b7838e21b81d5bb55fcf75eae85f62cce1e34b9ca71f8116a2c802eb8e1265446ea0d6caf05  Private.SourceBuilt.Artifacts.0.1.0-7.0.100-bootstrap.11.noextract
29a067c8ce1e7a7501423c0340d508ef40f82a9abfdf552612bdb844cc41140d990fcc7c193cd1989d40e3c747ed0a5752ac507ee60a7795cded0a6fe3a9e156  llvm-project-c1304304028d603e34f6803b0740c34441f80d2e.tar.gz
f930ce1c7e3c41893819fe7270ba9d0735d39861d6723fbc3119d76202aab662854fb61406a6c72fd14e6644da23ff0a0f2a5d546454f9d0a4bbeed7b3a30b80  dotnet-versiondetails-v7.0.100-rc.1.22431.12.xml
2ede8d9352a51861a5b2550010ff55da8241381a6fa6cc49e025f1c289b230b8c0177e93850de4ea8b6f702c1f2d50d81a9f4d890ca9441c257b614f2a5e05dd  stylecop.analyzers.1.2.0-beta.435.nupkg
f51e39a1821df5aa9f0e80f90ecd287991850baafa49e8ef5ac45a32c36b48a0c93ec3012c575ed7ad25ec867a51ce31f189dfaf2bcd8214c1626a5fea1e58b8  stylecop.analyzers.unstable.1.2.0.435.nupkg
4394debb3cfec7bec7f223029c8283fd57b28ad765d47f354c7ca667756cbfc1ae4a51b216a0f81452400f60b58f08347bac327cd717a1947dad737b37cbe3ef  aspnetcore_musl-build-fix.patch
44b5988fc3b2a07c55f006b6d06b6b2fc557c2dfebd6af0bbba0c9fb71a88d6b2e8170fab0e1db9f65ea789256a7965d2c959fb3f81606d905c5df54062914dc  aspnetcore_supress-ca1305.patch
23b712d7a088e780cd0994c5577b4c015cc1478e3a6bbca9b6e97ff2f2711562ef3a01ed483582a6861b31989b949972e2159caa4a34e0b18b6e8c062bb82c9e  installer_arm-build.patch
084665c29669df7a5a8db0256d39abc4f4dc2512ab4980ea9b208072c945b63493378c3aeb8fc004ded4891627af4f324fc3a35d3f052602b11cb5c459227637  installer_musl-build-fix.patch
8ef14a5e896c014d10db7c9458244a37ee947b43f9e015da44e500b88d79e36eaf35ba32198e671ca036594e77cf65caac462b176ca9eef19eaa942b3e1862b8  installer_reprodicible-tarball.patch
cd5d918e1ce41eb4ad66e9e9a872acad587a5dce6e0331606df27c9225b0267cb92620c8a6d482126ddbd929e5b33a3f51244df89c03b50d40d0da8d3574dc1f  installer_runtimepacks.patch
69b10a08f355d28222f64648c949bfca6a55a3480307211ed24db3ecb14fee60281772021f634e5f9d784b2f0483f096a8df7b76a8d4a41a034d411291782b2f  installer_set-local-repo.patch
87c5c7459781da13c898578e592a2483bc99e6bfd1904d8ef31d0e369350fc371982b2386cf77d4fd2b0bb46948b82680162cd5aa15ed831fc546858eb29fede  roslyn_revert-lift-version-codeanalysis.patch
22a9b5e39da2cd9af2fbcbe1a0922c7dfbfdd8df416af4fa2ec24f869a59716f04b83fdb18f8a08abb4ae5182fb7f7e85049e16a6c6289a4340cae41e1136562  runtime_add-cmake-asm-workaround.patch
a997a868393636c69df5228bc6c5757d9b303152378feb695098d75e3d2c721bcc46e0036473defa883f016e0d91940c22736913f985b26e7bf9f48475cfdce5  sdk_dummyshim-fix.patch
306f650ea3fa5bbb7dcd6447fb0be9f8f64b67095580bf247449dc1949696c2444e2b5b70605625bcdcc38e1d53da0aba88d3bfda0d4a83383f49b036e9995b8  sdk_set-local-repo.patch
"
