# DEPRECATION
This has now been re-implemented as a new stage0 in `alpine/aports:community/dotnetx-stage0`. This repo is now archived.

# Description
APKBUILD to crossbuild a slim-down version of dotnet6#

# Generated packages
N/A

# Artifacts
A tarball will be generated, allowing dotnet6-stage0 to pull for a complete build of dotnet6.
